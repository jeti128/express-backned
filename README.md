# Base project for Express

## Depend
#### TypeScript
#### Node.js
#### Express
#### Mongoose
#### nodeMailer
<br>
<br>

## Commands

#### start server developer mode
```
    npm run start:dev
```

#### testing
```
    npm test
```

#### testing not clear test db
```
    npm run test:nc
```
