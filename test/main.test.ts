import "module-alias/register";
import { Config } from "@config/config";
import { httpService, RequestType } from "./test.client";
import { TestConfig } from "@config/configtest";

import { server } from "../src/server";
import { userTest } from "./user/user";

import { singupTest } from "./user/singup";

let testServer: server;
let config = Config.getConfig();
var d = new Date();
let token: string = ``;

config.mongoDB += `_test`;

const baseURL = TestConfig.backendLink;


describe(`server test`, () => {
  test(`start server`, async () => {
    testServer = new server(config);
    await testServer.startServer();
  });

  test(`Test server work`, async () => {
    expect((await httpService(RequestType.get, `${baseURL}/`)).data).toEqual(
      `Hello word!`
    );
  });
});

singupTest();
userTest();

test(`stop server`, async () => {
  await testServer.stopServer();
});
