import { TestConfig } from "@config/configtest";
import { httpService, RequestType } from "../test.client";
const baseURL = TestConfig.backendLink;

export async function singupTest() {
  describe(`Singup fail tests`, () => {
    test(`Singup fail test (e-mail field not exists)`, async () => {
      let data = await httpService(RequestType.post, `${baseURL}/user/singup`, {
        firstName: `Janovicz`,
        lastName: `Gabor`,
        password: `Abcde1234`,
        userName: `Jeti128`,
      });

      expect(data.status).toEqual(400);
      expect(data.data).toEqual({
        error: `email or username or password field not found`,
      });
    });

    test(`Singup fail test (userName field not exists)`, async () => {
      let data = await httpService(RequestType.post, `${baseURL}/user/singup`, {
        firstName: `Janovicz`,
        lastName: `Gabor`,
        password: `Abcde1234`,
        email: `jeti128@gmail.com`,
      });
      expect(data.status).toEqual(400);
      expect(data.data).toEqual({
        error: `email or username or password field not found`,
      });
    });

    test(`Singup fail test (password field not exists)`, async () => {
      let data = await httpService(RequestType.post, `${baseURL}/user/singup`, {
        firstName: `Janovicz`,
        lastName: `Gabor`,
        userName: `Jeti128`,
        email: `jeti128@gmail.com`,
      });
      expect(data.status).toEqual(400);
      expect(data.data).toEqual({
        error: `email or username or password field not found`,
      });
    });
  });
};
