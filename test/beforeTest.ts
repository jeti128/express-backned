import 'module-alias/register';
import { Config } from "@config/config";
import { UserModel } from "@models/user.model";
import { DBConnect } from "../src/connect";

let config = Config.getConfig();

config.mongoDB += `_test`;
let connect = new DBConnect(config);

async function dbConnecting(): Promise<void> {
    await connect.connecting();
}

async function dbDisconnect(): Promise<void> {
    await connect.closeConnect();
}

async function setupData(): Promise<void> {
    let newUser = new UserModel({
        firstName: `Janovicz`,
        lastName: `Gabor`,
        password:  UserModel.hashPassword(`Abcde1234`),
        userName: `Jeti128`,
        email: `admin@jeti128.com`,
        status: 2,
        roles: [`login`]
      });

      await newUser.save();
}

async function start(){
    await dbConnecting();
    await setupData();
    await dbDisconnect();
}

start();