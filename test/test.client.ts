import axios, { AxiosResponse } from "axios";

export enum RequestType {
  get = 0,
  post = 1,
}

export function httpService(
  type: RequestType,
  url: string,
  data?: any,
  config?: any
): Promise<AxiosResponse> {
  return new Promise((resolve, reject) => {
    if (type === RequestType.get) {
      axios
        .get(url, config)
        .then((res) => {
          resolve(res);
        })
        .catch((error) => {
          resolve(error.response);
        })
    }

    if (type === RequestType.post) {
      axios
        .post(url, data, config)
        .then((res) => {
          resolve(res);
        })
        .catch((error) => {
          resolve(error.response);
        });
    }
  });
}
