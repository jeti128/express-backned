import 'module-alias/register';
import { Config } from '@config/config';
import { server } from 'server';

let runingServer = new server(Config.getConfig());

async function start() {
    await runingServer.startServer();
    //await runingServer.stopServer();
}

start();
