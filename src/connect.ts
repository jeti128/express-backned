import { Config } from "@config/config";
import { IConfig } from "@models/config.model";
import { connect, connection, Mongoose } from "mongoose";

export class DBConnect {
  private mongodbLink: string = ``;
  private config: IConfig;

  constructor(config: Config) {
    this.config = config;
    this.mongodbLink = this.config.mongoDB;
  }

  public async connecting(): Promise<boolean> {
    let client:Mongoose 

    try {
      client = await connect(this.mongodbLink, {});
    } catch (error) {
      return false;
    }

    if (!client) {
        //console.log(`Error to connect mongodb: ${this.mongodbLink}`);
        return false;
    }

    //console.log(`Connect succeful to: ${this.mongodbLink}`);
    return true;
  }

  public closeConnect(): void {
    connection.close()
  }
}
