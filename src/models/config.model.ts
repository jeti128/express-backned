export interface IConfig {
    jwtSecret: string;
    serverPort: number;
    mongoDB: string;
    mail_host?: string;
    mail_port?: number;
    mail_user?: string;
    mail_password?: string;
    mail_from?: string;
}