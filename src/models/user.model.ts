import { Schema, Document, Model, model} from 'mongoose';
import * as bcrypt from 'bcryptjs';

export interface IBaseForLogin {
    firstName: string;
    lastName: string;
    userName: string;
    email: string;
    status?: number;
    roles?: Array<string>;
    token?: string;
}
export interface IBaseUser extends IBaseForLogin {
    password: string;
    tokens?: Array<string>;
}

export interface IUser extends IBaseUser, Document {
    passwordCheck(password: string):Promise<boolean>;
    loginUser(token:string): Promise<IBaseForLogin>;
    hashPassword(password: string ): string;
    convertToBasUser(userData: IUser): IBaseForLogin;
}

export const UserSchema = new Schema({
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    userName: {
        type: String,
        required: false,
        default: ''
    },
    email: {
        type: String,
        required: true,
        unique: true,
    },
    password: {
        type: String,
        required: true,
    },
    status: {
        type: Number,
        required: false,
        default: 0
    },
    roles: {
        type: [String],
        required: false,
        default: []
    },
    tokens: {
        type: [String],
        required: false,
        default: []
    },
}, {
    "minimize": false,
    "timestamps": true,
    "collection": 'Users',
    'toJSON': { "getters": true, "virtuals": true },
    'toObject': { "getters": true, "virtuals": true }
});

UserSchema.methods.passwordCheck = async function(password: string): Promise<boolean> {
    return bcrypt.compareSync(password, this.password);
}

UserSchema.methods.loginUser = async function(token:string): Promise<IBaseForLogin> {
    this.tokens.push(token);
    await this.save()
    return {
        firstName: this.firstName,
        lastName: this.lastName,
        userName: this.userName ? this.userName : '',
        email: this.email,
        status: this.status,
        roles: this.roles ? this.roles : [],
        token: token
    };
}
UserSchema.statics.convertToBasUser = function(userData: IUser): IBaseForLogin {
    return {
        firstName: userData.firstName,
        lastName: userData.lastName,
        userName: userData.userName,
        email: userData.email,
        roles: userData.roles,
        status: userData.status
    };
}

UserSchema.statics.hashPassword = function(password: string ): string {
    return bcrypt.hashSync(password);
}

export interface User extends Model<IUser> {
    passwordCheck(password: string):Promise<boolean>;
    loginUser(token:string): Promise<IBaseForLogin>;
    hashPassword(password: string ): string;
    convertToBasUser(userData: IUser): IBaseForLogin;
}

export const UserModel = model<IUser, User>('Users', UserSchema);
