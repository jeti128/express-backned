import express from "express";
import * as bodyParser from "body-parser";
import cors from "cors";
import helmet from "helmet";
import { rootRouter } from "@routers/root.router";
import { DBConnect } from "./connect";
import { Config } from "@config/config";
import { IUser } from "@models/user.model";

declare global {
  namespace Express {
    interface Request {
      user?: IUser
    }
  }
}

export class server {
  private app: any = express();
  private server: any;
  private dbConnect: DBConnect | null = null;
  private config: Config;

  constructor(config: Config) {
    this.config = config;
  }

  public async startServer(): Promise<boolean> {
    this.dbConnect = new DBConnect(this.config);
    await this.dbConnect.connecting();

    this.app.use(cors());
    this.app.use(helmet());
    this.app.use(bodyParser.urlencoded({ extended: false }));
    this.app.use(bodyParser.json());
    this.app.use(`/`, rootRouter);

    this.server = this.app.listen(this.config.serverPort);
    if (!this.server) {
      //console.log(`Server can't started`);
      return false;
    }
    //console.log(`server started at http://localhost:${port}`);

    return true;
  }

  public async stopServer(): Promise<void> {
    this.dbConnect?.closeConnect();
    await this.server.close();
  }
}
