import { Router, Request, Response } from "express";
import { userRouter } from "./user/user.router";
import { userAuthedRouter } from "./user/user.authed.router";

export const rootRouter = Router();

rootRouter.use(`/user`, userRouter)
rootRouter.use(`/userAuthed`, userAuthedRouter)


rootRouter.get("/", (request: Request, response: Response) => {
  response.send("Hello word!");
});

