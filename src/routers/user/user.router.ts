import { Router, Request, Response } from "express";
import { UserController } from "@controllers/user.controller";

export const userRouter = Router();

userRouter.get("/", (request: Request, response: Response) => {
  response.send("Hello world login!");
});

userRouter.post('/login',UserController.login);
userRouter.post(`/singup`,UserController.Singup);
