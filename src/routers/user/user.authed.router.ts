import { Router, Request, Response } from "express";
import { UserController } from "@controllers/user.controller";
import { checkJwt } from "@middleware/checkJwt";

export const userAuthedRouter = Router();

userAuthedRouter.use(`/*`, checkJwt);

userAuthedRouter.get(`/getUserData`, UserController.getUserData);
userAuthedRouter.post(`/changePassword`, UserController.changePassword);
userAuthedRouter.post(`/edit`,UserController.edit);