//import { Permissions } from "@models/rules.model";
import { IUser } from "@models/user.model";
import { Request, Response, NextFunction } from "express";
//import { getRepository } from "typeorm";

//import { User } from "../entity/User";

export const checkRole = (roles: Array<string>) => {
  return async (req: Request, res: Response, next: NextFunction) => {
    let userData: IUser = req.user as IUser;

    if (userData && userData.roles) {
      console.log(`Roles found`);
      for (let i = 0; i < roles.length; i++) {
        if (userData.roles.indexOf(roles[i]) === -1) {
          console.log(`not found `)
          res.status(401).send();
          return;
        }
      }
      next();
    } else {
      res.status(401).send();
    }

    
    /*
      if (roles.indexOf(userData.roles) > -1) next();
      else res.status(401).send();
    /*
    //Get the user ID from previous midleware
    const id = res.locals.jwtPayload.userId;

    //Get user role from the database
    const userRepository = getRepository(User);
    let user: User;
    try {
      user = await userRepository.findOneOrFail(id);
    } catch (id) {
      res.status(401).send();
    }

    //Check if array of authorized roles includes the user's role
    if (roles.indexOf(user.role) > -1) next();
    else res.status(401).send();
    */
  };
};
