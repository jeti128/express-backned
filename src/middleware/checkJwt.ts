import { Request, Response, NextFunction } from "express";
import * as jwt from "jsonwebtoken";
import { Config } from "@config/config";
import { UserModel } from "@models/user.model";

let config = Config.getConfig();

let jwtSecret:string = config.jwtSecret;


export const checkJwt = async (req: Request, res: Response, next: NextFunction) => {
  //Get the jwt token from the head
  const token = <string>req.headers["authorization"];
  if (!token) {
    res.status(401).send(`not found token`);
    return;
  }
  let jwtPayload;
  
  //Try to validate the token and get data
  try {
    jwtPayload = <any>jwt.verify(token.split(` `)[1], jwtSecret);
    res.locals.jwtPayload = jwtPayload;
  } catch (error) {
    //If token is not valid, respond with 401 (unauthorized)
    res.status(401).send(`invalid token`);
    return;
  }

  //The token is valid for 1 hour
  //We want to send a new token on every request
  /*
  const newToken = jwt.sign({ userId, username }, jwtSecret, {
    expiresIn: "1h"
  });
  res.setHeader("token", newToken);
  */
  //Call the next middleware or controller
  const { userId, email } = jwtPayload;

  let userData = await UserModel.findOne({_id: userId}).exec();

  if (!userData) {
    res.status(401).send(`server not found user`);
    return;
  } else {
    req.user = userData;
  }
  
  next();
};