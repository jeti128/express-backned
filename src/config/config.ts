import { IMailConfig } from "@libs/email.client";
import { IConfig } from "@models/config.model";

let config: Config | null = null;

export class Config implements IConfig {
  public jwtSecret = "Aztamekkoraegykamu";
  public serverPort = 8080;
  public mongoDB = `mongodb://localhost:27017/jTWoWLokingForGroup`;
  public mail_host = `smtp.digikabel.hu`;
  public mail_port = 25;
  public mail_user = `jeti128@digikabel.hu`;
  public mail_password = `b8HGx7TL`;
  public mail_from = `jeti128@digikabel.hu, jeti128@digikabel.hu`;
  /*
  constructor(config?: IConfig) {
    if (config) {
      this.jwtSecret = config.jwtSecret
        ? config.jwtSecret
        : `aztamekkoraegyfassag`;
        this.serverPort = config.serverPort
        ? config.serverPort
        : 8080;
        this.mongoDB = config.mongoDB
        ? config.mongoDB
        : ``;
    }
  }*/

  public static getConfig(): Config {
    if (!config) {
      config = new Config();
    }

    return config;
  }

  public getMailConfig(): IMailConfig {
    return {
      host: this.mail_host,
      port: this.mail_port,
      user: this.mail_user,
      password: this.mail_password,
      from: this.mail_from
    }
  }
}
