/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
const { pathsToModuleNameMapper } = require('ts-jest')
const { compilerOptions } = require('./tsconfig')

module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  roots: ['<rootDir>'],
  modulePaths: ['<rootDir>/src'],
  modulePathIgnorePatterns: ["<rootDir>/dst/"],
  moduleNameMapper: pathsToModuleNameMapper(compilerOptions.paths),
}